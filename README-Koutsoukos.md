## Instructions

My solution consists of two main files:
1. start-tcp-server.py
2. BookHandler.py

Parse the books and create a pickle file (words_dictionary.pickle) by running BookHandler.py. Alternatively, run start-tcp-server.py, after setting 'rebuild_json' to True.

Run start-tcp-server.py to load the file and run the server.

## Report


