import json
import ntpath
import os
import pickle
import sys
import time

from collections import Counter
from itertools import chain
from operator import itemgetter
from string import punctuation


class BookHandler:
    book_dir = 'books_all/'
    _book_dir = 'books/'

    def __init__(self):
        pass

    @staticmethod
    def count_words_in_files():
        """Counts the words in each book and stores them in a json file, with a format dict
         {word: [{'book': book_name1, 'count': number },{'book': book_name2, 'count': number },...]}
         sorted by word occurrences in each book"""
        word_dict = {}

        # build words dictionary
        start_time = time.time()
        dir_len = len(os.listdir(BookHandler.book_dir))
        i = 0
        for book in os.listdir(BookHandler.book_dir):
            book_path = os.path.join(BookHandler.book_dir, book)
            i += 1
            sys.stdout.write('\r Processing Book: {0}. {1} of {2}'.format(book_path, i, dir_len))
            sys.stdout.flush()

            with open(book_path) as f:
                book_name = ntpath.basename(book_path)
                # line_words = (re.split('[^a-zA-Z]+', line.translate(None, punctuation).lower().strip()) for line in f)
                line_words = (line.translate(None, punctuation).lower().strip().split() for line in f)

                this_book = Counter(chain.from_iterable(line_words))

                for word in this_book:
                    if word:
                        if word not in word_dict:
                            word_dict[word] = []
                            word_dict[word].append({'book': book_name, 'count': this_book[word]})
                        # if word exists in dictionary
                        else:
                            # try to find the book in which it exists and add 1 count
                            get_book = next((item for item in word_dict[word] if book_name in item['book']), None)
                            if get_book is None:
                                word_dict[word].append({'book': book_name, 'count': this_book[word]})
                            else:
                                get_book['count'] += this_book[word]

        print('\rParsing done in --- %s seconds ---' % (time.time() - start_time))

        start_time = time.time()
        print('\rSorting words...')
        # sort list of words by count value
        for word in word_dict:
            word_dict[word] = sorted(word_dict[word], key=itemgetter('count'), reverse=True)
        print('\rSorting done in --- %s seconds ---' % (time.time() - start_time))

        start_time = time.time()
        print('\rWriting dictionary to file...')
        # write dictionary to json file
        with open('words_dictionary.pickle', 'wb') as new_file:
            # data = json.dumps(unicode(word_dict))
            # data = json.dumps(word_dict, indent=1, ensure_ascii=False)
            pickle.dump(word_dict, new_file)
            # data = json.dumps(word_dict, ensure_ascii=False)
            # new_file.write(data)
        print('\rFile created in --- %s seconds ---' % (time.time() - start_time))

        # with open('new_dict.json', 'w') as new_file:
        #     new_file.write(json.dumps(word_dict))

        return len(word_dict)

    @staticmethod
    def count_total(w_dict):
        """Counts total occurrences of each word in every book and
        stores them in a dict {word:total_occurrences}
        :param w_dict:
        :return words_count:
        """
        start_time = time.time()

        words_count = {}
        for word in w_dict:
            total = 0
            for item in w_dict[word]:
                total += item['count']

            words_count[word] = total

        print('---Counted words in %s seconds ---' % (time.time() - start_time))

        return words_count


if __name__ == '__main__':
    number_of_words = BookHandler.count_words_in_files()

    print '\rUnique words: ', number_of_words
