# encoding: utf-8
import SocketServer
import codecs
import json
import pickle
import time

import io

import BookHandler


class MyTCPHandler(SocketServer.StreamRequestHandler):
    rebuild_json = False

    def handle(self):
        _handlers = {'common': self.common_cmd,
                     'search': self.search_cmd}
        data = self.rfile.readline().strip()
        l = data.split()
        if len(l) >= 2:
            command, args = l[0], l[1:]
            f = _handlers.get(command, None)
            if f:
                ans = f(*args)
            else:
                ans = 'Invalid command'
        else:
            ans = 'Invalid Usage'
        self.request.sendall(ans + '\r\n')

    def common_cmd(self, *args):
        """Returns the n most words in the books.
        Sorts the words_count dictionary and returns the result."""

        common_time = time.time()

        ans = []
        i = int(args[0])
        sorted_list = sorted(words_count.iteritems(), key=lambda (k, v): (v, k), reverse=True)[:i]
        for w in sorted_list:
            ans.append('{} {}'.format(w[0].encode('utf-8').strip(), w[1]))

        print("---Completed 'common' command in %s seconds ---" % (time.time() - common_time))

        return '\n'.join(ans)

    def search_cmd(self, *args):
        """Searches for the given word in the words_list dict.
        Returns a string with the name of the occurrences of the word in each book."""

        search_time = time.time()

        ans = []

        # query =  #.decode('utf-8')

        if args[0] in words_list:
            for book in words_list[args[0]]:
                ans.append('{} {}'.format(book['book'], book['count']))

        print("---Completed 'search' command in %s seconds ---" % (time.time() - search_time))

        return '\n'.join(ans)


if __name__ == "__main__":
    """Starts the TCP Server. 
    If 'MyTCPHandler.rebuild_json' is True, rebuilds the json file that contains the word dictionary,
    using the BookHandler class. Loads the json file.
    Counts the total word occurrences and stores then in a dictionary
    {word1: total_count, word2: total_count, ...}"""

    print '---Started---'

    if MyTCPHandler.rebuild_json:
        BookHandler.BookHandler.count_words_in_files()

    start_time = time.time()

    # load the json file with the words

    # file_data = open('words_dictionary.json', 'r').read()
    # words_list = json.loads(file_data, 'cp1250')

    # file_data = open('words_dictionary.json', 'r').read()# encode('utf8').decode('utf8')

    with open('words_dictionary.pickle', 'rb') as file:
        words_list = pickle.load(file)

    print('---Loaded json in %s seconds ---' % (time.time() - start_time))

    # count words
    words_count = BookHandler.BookHandler.count_total(words_list)

    print '---Listening... ---'

    # start server
    HOST, PORT = "0.0.0.0", 9999

    server = SocketServer.TCPServer((HOST, PORT), MyTCPHandler)
    server.serve_forever()

